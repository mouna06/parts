import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { RequetesComponent } from './requetes/requetes.component';
import { PublicComponent } from './public/public.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';

const routes: Routes = [
  {path:'', component: AccueilComponent},
  {path:'requetes', component: RequetesComponent},
  {path:'public', component: PublicComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CollapseModule.forRoot()],
  exports: [RouterModule]
})
export class AppRoutingModule { }
