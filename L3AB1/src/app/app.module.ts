import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AlertModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';

import { AccueilComponent } from './accueil/accueil.component';
import { CopyrightComponent } from './copyright/copyright.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PublicComponent } from './public/public.component';
import { RequetesComponent } from './requetes/requetes.component';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    CopyrightComponent,
    NavigationComponent,
    PublicComponent,
    RequetesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
